#/usr/bin/env python

import sys
import argparse
import numpy as np
from scipy.integrate import dblquad
from scipy.interpolate import interp1d

# Printing things (warnings, errors and a progress bar)

def print_warning_message(message):
    print("WARNING: "+message)

def print_error_message_and_exit(message):
    print("ERROR: "+message)
    raise SystemExit

def progressbar(it, prefix="", suffix="", size=60, file=sys.stdout):
    count = len(it)
    def show(j):
        x = int(size*j/count)
        file.write("%s [%s%s] %i/%i %s\r" % (prefix, "-"*x, " "*(size-x), j, count, suffix))
        file.flush()
    show(0)
    for i, item in enumerate(it):
        yield item
        show(i+1)
    file.write("\n")
    file.flush()

# All integrands

def dPdtaudmu(tau, mu):
    # Last equation inside Beloborodov (2011)
    if mu == -1.:
        return 0.
    if tau == 0.:
        A = 1.
    else:
        A = 1.5 + (1./np.pi)*np.arctan(1./3.*(tau - 1./tau))
    tau_ray = tau/6.*(3. + (1. - mu)/(1. + mu))
    f = .25*A*np.exp(-tau_ray)
    return f

def integrand_spectrum(mu, tau, E, E_bs, N_E_stars, xi_function, xi_norm):
    # The integrand for the output spectrum
    if mu == -1:
        return 0.
    f = dPdtaudmu(tau, mu)
    xi = xi_norm*xi_function(tau)
    E_star = E/(1.+mu)/xi
    N_E_star = N_E_stars(E_star)
    return f/(1.+mu)/xi*N_E_star

def integrand_normalization(mu, tau, xi_function):
    # Normalizing the xi_function means that the average photon energy is conserved by the integration
    f = dPdtaudmu(tau, mu)
    s = xi_function(tau)
    return f*(1.+mu)*s

# Best fit xi function

def xi_best_fit(tau):
    return tau**(2./3.) + .2

# The Decoupler class, which will load spectra, integrate them, and output new spectra

class Decoupler():
    def __init__(self):
        # Input spectrum information
        self.input_file_names = []
        self.header_liness = []
        self.spec_unitss = []
        self.Ess = []
        self.N_Ess = []

        # Output spectrum information
        self.output_file_names = []
        self.N_Ess_o = []

        # The "xi" function
        self.xi = None
        self.xi_norm = 1.

    def set_xi_function(self, func, epsrel=1e-2):
        # Sets the xi function, which is necessary for the integration
        self.xi = func
        A = dblquad(integrand_normalization, 0., np.infty, lambda tau: -1., lambda tau: 1., args=[self.xi], epsrel=epsrel)[0]
        self.xi_norm = 1./A

    def load_input_spectra(self, file_name, output_file_name_from_args, spec_units, batch_mode=False):
        # Get input file names
        if batch_mode:
            for input_file_name in self._get_input_file_names_from_batch_file(file_name):
                self.input_file_names.append(input_file_name)
        else:
            self.input_file_names.append(file_name)

        # Make the output file names
        if not output_file_name_from_args:
            for input_file_name in self.input_file_names:
                i = input_file_name.rfind('.')
                output_file_name = input_file_name[:i]+'_d'+input_file_name[i:]
                self.output_file_names.append(output_file_name)
        else:
            self.output_file_names.append(output_file_name_from_args)

        # Load each spectrum
        for input_file_name in self.input_file_names:
            try:
                with open(input_file_name, 'r') as f:
                    Es = []
                    N_Es = []
                    header_lines = []
                    for line in f.readlines():
                        add_header_line = True
                        line_s = line.split()
                        if len(line_s) == 2:
                            try:
                                E = float(line_s[0])
                                N_E = float(line_s[1])

                                if spec_units == 1:
                                    N_E = N_E/E
                                elif spec_units == 2:
                                    N_E = N_E/E/E
                                Es.append(E)
                                N_Es.append(N_E)
                                add_header_line = False
                            except:
                                pass

                        if add_header_line:
                            header_lines.append(line)

                    # Store loaded information
                    self.header_liness.append(header_lines)
                    self.spec_unitss.append(spec_units)
                    self.Ess.append(Es)
                    self.N_Ess.append(N_Es)

            except:
                print_warning_message('Could not open file "%s"' % (file_name))

    def integrate(self, epsrel=1e-2):
        # Is the xi function set?
        if not self.xi:
            print_error_message_and_exit("Can't performe the integration; no xi function was set.")

        # Integrate the loaded input spectra
        j = 0
        for Es, N_Es, input_file_name, output_file_name in zip(self.Ess, self.N_Ess, self.input_file_names, self.output_file_names):
            j += 1
            N_Es_o = []
            for E in progressbar(Es, "Integrating spectrum %i/%i" % (j, len(self.Ess)), "(%s -> %s)" % (input_file_name, output_file_name)):
                E_bs = self._make_boundaries(Es)
                N_Es_interp = interp1d(Es, N_Es, fill_value=0., bounds_error=False, kind='linear')
                N_E_o = dblquad(integrand_spectrum, 0., np.infty, lambda tau: -1., lambda tau: 1., args=(E, E_bs, N_Es_interp, self.xi, self.xi_norm), epsrel=epsrel)[0]
                N_Es_o.append(N_E_o)
            self.N_Ess_o.append(N_Es_o)

        # Check if photon number and energy is conserved
        self._check_conservation()

    def save_output_spectra(self):
        # If no integration has occured, print error and exit
        if not self.N_Ess_o:
            print_error_message_and_exit("No integrated spectra to save.")
        for Es, N_Es_o, output_file_name, spec_units, header_lines in zip(self.Ess, self.N_Ess_o, self.output_file_names, self.spec_unitss, self.header_liness):
            with open(output_file_name, 'w') as f:

                # Write header lines from the input spectra
                for line in header_lines:
                    f.write(line)

                # Write the data
                for E, N_E_o in zip(Es, N_Es_o):
                    if spec_units == 1:
                        N_E_o = N_E_o*E
                    elif spec_units == 2:
                        N_E_o = N_E_o*E*E
                    string = ' %12.8e %12.8e\n' % (E, N_E_o)
                    f.write(string)

    def _check_conservation(self):
        # Computes the number of photons and the average photon energies for input and output spectra
        # Prints a warning if the differences are large
        j = 0
        for Es, N_Es, N_Es_o in zip(self.Ess, self.N_Ess, self.N_Ess_o):
            j += 1
            N = 0.
            B = 0.
            N_o = 0.
            B_o = 0.
            E_bs = self._make_boundaries(Es)
            for i in range(len(N_Es)):
                E = np.sqrt(E_bs[i+1]*E_bs[i])
                dE = E_bs[i+1]-E_bs[i]
                N += N_Es[i]*dE
                B += E*N_Es[i]*dE
                N_o += N_Es_o[i]*dE
                B_o += E*N_Es_o[i]*dE
            E_bar = B/N
            E_bar_o = B_o/N_o
            dE_bar_over_E_bar = abs(E_bar_o-E_bar)/E_bar
            dN_over_N = abs(N_o-N)/N
            if dN_over_N > .1 or dE_bar_over_E_bar > .1:
                print_warning_message('Spectrum %i conservation violated at the magnitude of dN/N = %4.4e, dE_bar/E_bar = %4.4e' % (j, dN_over_N, dE_bar_over_E_bar))

    def _make_boundaries(self, Es):
        # Assuming EVEN logarithmic spacing in E, and constructs the bin boundaries
        E_bs = [np.sqrt(Es[i+1]*Es[i]) for i in range(len(Es)-1)]
        E_bs.insert(0, E_bs[0]/(E_bs[1]/E_bs[0]))
        E_bs.append(E_bs[-1]*(E_bs[-1]/E_bs[-2]))
        return E_bs

    def _get_input_file_names_from_batch_file(self, input_file_name):
        # Simply extracts the file names
        with open(input_file_name, 'r') as f:
            file_names = []
            for line in f.readlines():
                if line[-1] == '\n':
                    line = line[:-1]
                if line:
                    file_names.append(line)
        return file_names

# Definition of input arguments

def get_args():

    # Get terminal input
    parser = argparse.ArgumentParser(description='takes an input spectrum and computes (integrates) the broadened spectrum that would result after the input spectrum decouples from the photosphere of a coasting, spherical, relativistic flow. the broadened spectrum contains the same photon number and energy as the input spectrum. by default, the output spectrum gets the same file name as the input spectrum, but with "_d" added before the file extension. an even logarithmic spacing in energy is assumed.')
    parser.add_argument('INPUT_SPECTRUM_FILE', help='a plain text file containing the input spectrum as two columns (first energy, second N_nu), OR a column of spectrum file names (for batch processing of many spectra with the --batch-mode argument). spectrum files can contain any number of "header lines" before the data; these lines will be carried over to the output files.')
    parser.add_argument('-b', '--batch-mode', action='store_true', help='assumes that the input file instead contains a column of spectrum file names for batch processing')
    parser.add_argument('-o', '--output-spectrum-file', help='optionally, choose the output spectrum file name (not supported for batch processing)')
    parser.add_argument('-s', '--spectrum-units', type=int, default=0, help='the units of the input (and output) spectra. choose between 0, 1 and 2: 0 -> N_nu (default), 1 -> F_nu, 2 -> nu F_nu.')
    args = parser.parse_args()

    if args.output_spectrum_file and args.batch_mode:
        print_error_message_and_exit("Output file names can't be chosen when batch processing multiple spectra.")
    return args

# The main function

def main():

    # Get args
    args = get_args()

    # Load and use the decoupler
    decoupler = Decoupler()
    decoupler.set_xi_function(xi_best_fit)
    decoupler.load_input_spectra(args.INPUT_SPECTRUM_FILE, args.output_spectrum_file, args.spectrum_units, args.batch_mode)
    decoupler.integrate(epsrel=3e-2)
    decoupler.save_output_spectra()

if __name__ == '__main__':
    main()
