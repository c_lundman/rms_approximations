import argparse
import numpy as np
from scipy.optimize import root_scalar

# Only need the ratio
m_p = 1836.
m_e = 1.

def print_parameter(string, value, format=None, prefix=None):
    if format == 'f':
        p_string = string+' = %.f' % (value)
    else:
        p_string = string+' = %8.4e' % (value)
    if prefix:
        p_string += ' '+prefix
    print(p_string)

def get_default_parameters():
    # Default input parameters
    theta_rms = 4.44444e-4
    R_theta = theta_rms/1e-6
    eps_d = 1.6353e-5
    k = 7.5
    return theta_rms, R_theta, eps_d, k

def solve_for_M_sq(M_sq, xi):
    # Function for computing the Mach number: should equal zero
    A = ((M_sq + 6.)/(7.*M_sq))**.3333
    # B = (8.*M_sq + 1.)*(M_sq + 6.)/49./M_sq
    B = (8.*M_sq - 1.)*(M_sq + 6.)/49./M_sq
    return xi - A*B

def get_M_sq(xi):
    # Computing the Mach number (squared)
    solution = root_scalar(solve_for_M_sq, args=(xi), x0=1e1, method='brentq', bracket=(1e0, 1e4))
    if not solution.converged:
        print('ERROR: root finding did not converge.')
        print('Exiting...')
        raise SystemExit
    M_sq = solution.root
    return M_sq
    
# def get_beta_u_new(theta_rms, M_sq, k_1):
#     beta_u = np.sqrt(84.)/3.*np.sqrt(theta_rms/k_1)*M_sq/(M_sq-1.)
#     return beta_u

def get_rms_parameters(theta_rms, theta_u_K, eps_d, k):
    # xi is the ratio of average photon energies in the "downstream" and "upstream" for the Kompaneets code
    xi = eps_d/3./theta_u_K
    M_sq = get_M_sq(xi)

    # The ratio of average photon energies in the downstream and upstream in the RMS code
    eps_d_over_eps_u = (8.*M_sq - 1.)*(M_sq + 6.)/49./M_sq

    # Estimating the "y-parameter" of the real RMS
    y_rms = np.log(eps_d_over_eps_u)
    print_parameter('eps_d/eps_u', eps_d_over_eps_u)
    print_parameter('y_rms', y_rms)

    # Computing beta_u
    beta_u = k*np.sqrt(4.*theta_rms/y_rms)
    print_parameter('beta_u', beta_u)

    # Computing beta_ud
    beta_ud = (M_sq-1.)/((7./6.)*M_sq)*beta_u

    # From M_sq, compute theta_u for the RMS code
    theta_u = theta_u_K*(((M_sq + 6.)/7./M_sq)**.3333)
    # print 3*theta_u, 3*theta_u_K
    # print (((M_sq + 6.)/7./M_sq)**.3333)

    # From M_sq, compute zeta = n_gamma/n_p
    zeta = (3./4.)*(m_p/m_e)*beta_u*beta_u/theta_u/M_sq
    return beta_ud, theta_u, zeta, beta_u

#

def get_args():
    # Get terminal input
    theta_rms, R_theta, eps_d, k = get_default_parameters()
    parser = argparse.ArgumentParser(description='converts Kompaneets parameters to the corresponding RMS parameters. (the default parameters give beta_ud=0.1, theta_u=1e-6, zeta=1e6.)')
    parser.add_argument('-tr', '--theta-rms', type=float, default=theta_rms, help='the RMS effective temperature [default=%4.4e' % (theta_rms)+']')
    parser.add_argument('-R', '--R-theta', type=float, default=R_theta, help='the temperature ratio, R_theta = theta_rms/theta_u [default=%4.4e' % (R_theta)+']')
    parser.add_argument('-ed', '--eps-d', type=float, default=eps_d, help='average downstream photon energy [default=%4.4e' % (eps_d)+']')
    parser.add_argument('-tt', '--tau-theta', type=float, default=None, help='the number of scatterings, times theta_rms [optional, no default value]')
    parser.add_argument('-k', type=float, default=k, help='y_rms = 4*theta_rms*(beta_u/k)^2, the only free parameter to fix by code comparisons. best value so far is 7.5 [default=%4.2f' % (k)+']')
    parser.add_argument('-pd', '--print-default', action='store_true', help='instead, print default parameter values and exit')
    parser.add_argument('-pp', '--print-parameters', action='store_true', help='instead, print current parameter values and exit')
    parser.add_argument('-pe', '--print-energies', action='store_true', help='instead, print characteristic energies and exit')
    parser.add_argument('--filip-y-05', action='store_true', help="automatically sets the parameters for Filip's y = 0.5 Kompaneets run")
    parser.add_argument('--filip-y-07', action='store_true', help="automatically sets the parameters for Filip's y = 0.7 Kompaneets run")
    parser.add_argument('--filip-y-3', action='store_true', help="automatically sets the parameters for Filip's y = 3 Kompaneets run (down-shifted by a factor 3 in energy)")
    parser.add_argument('--shift', type=float, default=None, help="shift the energies (theta_rms, eps_d) down by this factor")
    args = parser.parse_args()

    if args.filip_y_07:
        # -tr 3.3333e-4 -R 100 -ed 2.5631e-05 -tt 10
        args.theta_rms = 3.3333e-4
        args.R_theta = 100.
        args.eps_d = 2.5631e-05
        args.tau_theta = 10.
    elif args.filip_y_3:
        # -tr 1e-3 -R 300 -ed 3.9637e-04 -tt 10
        args.theta_rms = 1e-3
        args.R_theta = 300.
        args.eps_d = 3.9637e-4
        args.tau_theta = 10.
    elif args.filip_y_05:
        # -tr 1e-3 -R 10 -ed 4.8176e-04 -tt 10
        args.theta_rms = 1e-3
        args.R_theta = 10.
        args.eps_d = 4.8176e-04
        args.tau_theta = 10.

    if args.shift:
        args.theta_rms /= args.shift
        args.eps_d /= args.shift

    return args

def main():

    # beta_u = .7
    # xi = 55.
    # eps_d_over_eps_u = 1000.
    # theta_rms = beta_u*beta_u/4./xi*np.log(eps_d_over_eps_u)
    # print(theta_rms, 4*theta_rms)
    # raise SystemExit


    # Get the args
    args = get_args()

    # Print default values and exit?
    if args.print_default:
        print('default input parameters:')
        theta_rms, R_theta, eps_d, k = get_default_parameters()
        print_parameter('theta_rms', theta_rms)
        print_parameter('R_theta', R_theta)
        print_parameter('eps_d', eps_d)
        print_parameter('k', k)
        raise SystemExit

    # Set the parameters from the args
    theta_rms = args.theta_rms
    R_theta = args.R_theta
    eps_d = args.eps_d
    k = args.k
    theta_u_K = theta_rms/R_theta

    # Print current values and exit?
    if args.print_parameters:
        print('input parameters:')
        print_parameter('theta_rms', theta_rms)
        print_parameter('R_theta', R_theta)
        print_parameter('eps_d', eps_d)
        print_parameter('k', k)
        raise SystemExit

    # Compute the corresponding RMS parameters
    beta_ud, theta_u, zeta, beta_u = get_rms_parameters(theta_rms, theta_u_K, eps_d, k)

    # Print current values and exit?
    if args.print_energies:
        print('characteristic energies:')
        print_parameter('eps_max/eps_u_K', 4.*theta_rms/3./theta_u_K)
        print_parameter('eps_d/eps_u_K', eps_d/3./theta_u_K)
        print_parameter('eps_max/eps_d', 4.*theta_rms/eps_d)
        # print_parameter('eps_max/eps_u', 4.*theta_rms/3./theta_u)
        # print_parameter('eps_d/eps_u', eps_d/3./theta_u)
        raise SystemExit

    # Print output
    print('rms parameters (k=%3.1f):' % (k))
    print_parameter('beta_ud', beta_ud)
    print_parameter('theta_u', theta_u)
    print_parameter('Z_gamma', zeta)
    if args.tau_theta:
        t_over_t_sc = args.tau_theta/theta_rms/2.
        t_sc = .5
        t_f = t_sc*t_over_t_sc
        print(t_over_t_sc)

        m_p = 1.6726231e-24
        sigma_T = 6.6524e-25
        kappa_T = sigma_T/m_p
        rho = 1.
        tau_tot = kappa_T*rho*beta_u*t_f
        tau_tot += 2.*4./beta_u # adding two times shock starting location

        print_parameter('tau_tot', tau_tot, format='f')
        print_parameter(' -> t_f', t_f, format='f', prefix='(assuming t_sc = 0.5)')

if __name__ == '__main__':
    main()
