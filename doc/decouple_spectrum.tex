\documentclass[a4paper,11pt]{article}
\usepackage{amssymb,amsmath,graphicx,color}

\begin{document}

%% Macros
\newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}}

\section{Spectral broadening at the coasting photosphere}

The three-zone RMS model generates a spectrum at the jet photosphere, in the local comoving frame of the jet. This spectrum can be transformed to the lab frame using an approximate transformation with $D \approx \Gamma$, where $D$ is the Doppler boost and $\Gamma$ is the Lorentz factor of the jet. However, this transformation does not capture the broadening of the spectrum that occurs as the radiation decouples from the jet photosphere (refs). Photons make their final scattering at different radii and angles to the observer, and therefore experience different adiabatic energy losses and Doppler boosts. A full radiative transfer simulation would automatically take these effects into account. Here we apply these effects in a post-processing step on the approximately transformed spectra. The post-processing is explained in Appendix A.

\subsection{Appendix A: Post-processing the decoupling broadening}

Three ingedients are needed in order to post-process the transition to transparency: the local comoving spectral shape, the distribution of the last scattering radius and angle, and the radial cooling of radiation as the jet transitions to transparency. The spectral shape is here obtained from the three-zone RMS model, while the probability density for the last scattering radius ($r$) and comoving propagation direction ($\mu = \cos\theta$, where $\theta$ is the angle between the local radial direction and the observer line-of-sight) was computed by Beloborodov (2011), Equation B16. Finally, we calibrate the cooling to full transfer simulations, computed using \texttt{radshock} (ref Lundman). The broadened spectrum is then be computed by integrating the local spectrum over radius (or optical depth) and angle.

Assume that adiabatic cooling is described by a cooling function $\xi(\tau)$, so that the average comoving photon energy at $\tau$ is proportional to $\xi(\tau)$. At $\tau \gg 1$ we have $\xi \propto \tau^{2/3}$ (the usual adiabatic cooling), and at $\tau \ll 1$ we require $\xi$ to be constant as radiation streams freely. The observed receives emission from a distribution of radii, which broadens the spectrum. The Doppler boost also broadenes the spectrum, as a comoving photon energy is boosted as $E^\prime = D E$, where $D = [\Gamma(1 - \beta\tilde{\mu})]^{-1} = \Gamma(1 + \beta\mu)$, and $\tilde{\mu}$ is the lab frame transformation of $\mu$. For $\Gamma \gg 1$ we find $D \rightarrow \Gamma(1 + \mu)$. The Lorentz factor $\Gamma$ is here a constant, which will anyway drop out of the calculation at a later stage (when we require energy conservation below). Thus, we here define the Doppler boost to simply be

\be
D \equiv 1 + \mu.
\ee

Including both cooling and the Doppler boost, we define the lab frame energy of a photon to be

\be
E(\tau, \mu) = \left(1 + \mu\right) \xi(\tau) E_\star,
\ee

\noindent where $E_\star$ is the photon energy at the photosphere and $N_{E_\star}$ is the photon number spectrum, as computed by the un-processed three-zone RMS model. This energy transformation captures spectral broadening due to both radial (i.e. cooling) and angular (Doppler boost) effects.

The total number of photons is $N = \int N_{E_\star} dE_\star$. A spectrum with the same number of photons and spectal shape but ``shifted'' to spatial coordinates $\tau, \mu$ according to cooling and Doppler boost is

\be
N_E^s = N_{E_\star} \left|\frac{dE_\star}{dE}\right| = \frac{1}{(1+\mu) \xi(\tau)} N_{E_\star}.
\ee

\noindent Let $dP = f(\tau, \mu)d\tau d\mu$ be the probability that an observed photon originated from between $\tau, \tau + d\tau$ and $\mu, \mu + d\mu$. The local spectrum that originates from this location is then $dP N_E^s$. Integrating, the observed, broadened spectrum becomes

\be
N_E = \iint f(\tau, \mu) \frac{1}{(1 + \mu) \xi(\tau)} N_{E_\star} d\tau d\mu,
\ee

\noindent where $N_{E_\star}$ is evaluated at $E_\star = E/((1 + \mu) \xi(\tau))$ and the integrals have to be computed numerically. By construction, this transformation conserves photon number:

\be
\int N_E dE = \iint f(\tau, \mu) \int N_{E_\star} dE_\star d\tau d\mu = \int N_{E_\star} dE_\star = N,
\ee

\noindent since $\iint f(\tau, \mu) d\tau d\mu = 1$. Energy conservation is achieved by properly normalizing $\xi(\tau)$:

\be
\bar{E} = \frac{1}{N} \int E N_E dE = \bar{E}_\star \iint f(\tau, \mu) (1 + \mu) \xi(\tau) d\tau d\mu,
\ee

\noindent which requires

\be
\iint f(\tau, \mu) (1+\mu) \xi(\tau) d\tau d\mu = 1.
\ee

\noindent An arbitrary function $s(\tau)$ can be normalized into a valid cooling function $\xi(\tau)$ as

\be
\xi(\tau) = \frac{s(\tau)}{\iint f(\tau, \mu) (1 + \mu) s(\tau) d\tau d\mu}.
\ee

\noindent Informed by the output of full radiative transfer simulations, we find that

\be
s(\tau) = \tau^{2/3} + 0.2
\ee

\noindent reproduces the observed spectrum very well. The probability density function $f(\tau, \mu)$ is found by transforming equation B16 of Beloborodov (2011) using $\tau = R_\star/r$, which gives

\be
f(\tau, \mu) = \frac{1}{4}\left\{ \frac{3}{2} + \frac{1}{\pi} \arctan \left[ \frac{1}{3} \left( \tau - \tau^{-1} \right)\right]\right\} \exp \left[-\frac{\tau}{6}\left(3 + \frac{1 - \mu}{1 + \mu}\right)\right].
\ee

\end{document}
