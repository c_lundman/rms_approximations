\documentclass[a4paper,11pt]{article}
\usepackage{amssymb,amsmath,graphicx,color}

\begin{document}

%% Macros
\newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}}

\section*{Converting the Kompaneets parameters into RMS parameters}

The parameters for the Kompaneets solver are shown in Table~\ref{tab:kompaneets}, while the parameters for the full RMS simulation are shown in Table~\ref{tab:rms}. Here, we show how to convert the Kompaneets solver parameters to the corresponding RMS parameters.

Blandford \& Payne (1981) sowed that the total increase in average photon energy across the RMS is

\be
\frac{\bar{\epsilon}_d}{\bar{\epsilon}_u} = \frac{(8 M^2 + 1) (M^2 + 6)}{49 M^2},
\label{eq:eps_d_over_eps_u}
\ee

\noindent where $\bar{\epsilon}_u$ and $\bar{\epsilon}_d$ are the average photon energies in the up- and downstream respectively, and $M$ is the shock Mach number. Part of this energy gain is due to plasma compression across the shock, which increases the upstream energy by a factor $(7 M^2/[M^2 + 6])^{1/3}$ (Blandford \& Payne, 1981). The Kompaneets model can't account for compression. Therefore, in order to generate the same RMS spectrum, the codes need different upstream temperatures,

\be
\theta_u = \theta_{u, K} \left(\frac{M^2 + 6}{7 M^2}\right)^{1/3},
\label{eq:theta_u_over_theta_u_K}
\ee

\noindent where $\theta_u$ and $\theta_{u, K}$ are the upstream temperatures for \texttt{radshock} and the Kompaneets solver, respectively.

The average downstream photon energy, $\bar{\epsilon}_d$, can be found from the spectrum inside the RMS zone of the Kompaneets solver by integration. Knowing $\bar{\epsilon}_d$ and $\bar{\epsilon}_{u, K} = 3 \theta_{u, K}$ (where the last equality holds for a thermalized Wien spectrum) from the Kompaneets solver, we can compute the corresponding shock Mach number numerically from Equations~\ref{eq:eps_d_over_eps_u} and \ref{eq:theta_u_over_theta_u_K}, and then find $\theta_u$ from Equation~\ref{eq:theta_u_over_theta_u_K}.

The RMS transition region has a width that corresponds to an optical depth of $\tau_{sh} \sim 1/\beta_u$ (Blandford \& Payne, 1981), so that a typical photon does $\tau_{sh}^2 \sim 1/\beta_u^2$ scatterings as it crosses the shock. However, the typical photon does not scatter enough inside the RMS to reach $\bar{\epsilon}_d$. If we assume that the photons that reach energy $\bar{\epsilon}_d$ scatters $\xi$ times more than the typical photon, where $\xi \gg 1$ is expected, then

%% If we describe the energy gain inside the shock by an effective temperature, $\theta_{rms}$, then (in analogy with scatterings on thermal electrons) the photons will experience a $y$-parameter of

\be
%% \ln \left( \frac{\bar{\epsilon}_d}{\bar{\epsilon}_u} \right) = 4 \theta_{rms} \beta_u^2 \xi,
\bar{\epsilon}_d = \bar{\epsilon}_u \exp\left( 4 \theta_{rms} \beta_u^2 \xi \right),
\ee

%% \be
%% y_{rms} \sim 4\theta_{rms} \beta_u^{-2}
%% \ee

\noindent where (in analogy with scatterings on thermal electrons) $4 \theta_{rms} \equiv \Delta\epsilon/\epsilon$ is the relative energy gained per scattering. Using these approximate expressions and solving for $\beta_u$, we find

\be
%% \beta_u = k \left( \frac{4 \theta_{rms}}{\ln (\bar{\epsilon}_d / \bar{\epsilon}_u)} \right)^{1/2},
\beta_u^2 = \frac{4 \theta_{rms} \xi}{\ln (\bar{\epsilon}_d / \bar{\epsilon}_u)},
\label{eq:beta_u}
\ee

\noindent where $\xi$ is a constant (the only constant) to be calibrated by comparing simulated spectra from the two codes. \textbf{(I previously used the constant $k = \sqrt{\xi} \approx 7.5$.)}

It is not a-priori obvious that $\xi$ should be a constant, independent of the shock parameters. However, $\xi$ is simply related to the probability to scatter into the shock again after just exiting it, e.g. a geometrical parameter, which could be expected to be constant. By comparing the spectra produced by \texttt{radshock} and the three-zone model, we find that a constant value of $\xi = 55$ works very well across the shock parameter space.

The Mach number can be written as $M^2 = v_u^2 / c_s^2$, where the speed of sound is $c_s^2 = (4/3) p_u/\rho_u$ and $p_u/\rho_u = \theta_u c^2 (m_e n_\gamma)/(m_p n_p)$. Solving for the final parameter, $n_\gamma/n_p$, we find

\be
\frac{n_\gamma}{n_p} = \frac{3}{4} \frac{m_p}{m_e} \frac{\beta_u^2}{\theta_u M^2}.
\label{eq:n_gamma_over_n_p}
\ee

\noindent In summary, $\bar{\epsilon}_d$ is found by integration from the Kompaneets RMS zone spectrum, the shock Mach number is found numerically by combining Equations~\ref{eq:eps_d_over_eps_u} and \ref{eq:theta_u_over_theta_u_K}, and the RMS parameters are then found from Equations~\ref{eq:theta_u_over_theta_u_K}, \ref{eq:beta_u} and \ref{eq:n_gamma_over_n_p}.

\begin{table}
\centering
\caption{Three-zone Kompaneets model parameters.}
\begin{tabular}[t]{cl}
\hline
Parameter & Description \\
\hline
$\theta_{u, K}$ & Upstream zone radiation temperature \\
$\theta_{rms}$ & Effective electron temperature inside the RMS zone \\
$y$ & Determines the radiation power law inside the RMS zone \\
\hline
\label{tab:kompaneets}
\end{tabular}
\end{table}

\begin{table}
\centering
\caption{Real RMS parameters.}
\begin{tabular}[t]{cl}
\hline
Parameter & Description \\
\hline
$\theta_u$ & Upstream radiation temperature \\
$\beta_{ud}$ & Upstream speed relative to the downstream \\
$n_\gamma/n_p$ & Photon-to-proton ratio inside the plasma \\
\hline
\label{tab:rms}
\end{tabular}
\end{table}

\end{document}
